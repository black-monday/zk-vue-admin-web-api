FROM jdk20:v1 as zkapi
LABEL authors="xiadefeng"
VOLUME /app/docker-app/zk
ENV JAVA_OPTS null
COPY zkapi.jar zkapi.jar
EXPOSE 9797
ENTRYPOINT ["java","-jar","zkapi.jar","zkapi.log"]
