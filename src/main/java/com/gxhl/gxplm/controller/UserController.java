package com.gxhl.gxplm.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.gxhl.gxplm.common.exception.BusinessException;
import com.gxhl.gxplm.common.util.response.R;
import com.gxhl.gxplm.config.LUser;
import com.gxhl.gxplm.entity.dto.LoginDTO;
import com.gxhl.gxplm.entity.dto.UserDTO;
import com.gxhl.gxplm.entity.dto.UserPageDTO;
import com.gxhl.gxplm.entity.vo.LoginVO;
import com.gxhl.gxplm.entity.vo.UserPageVO;
import com.gxhl.gxplm.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@Slf4j
@Tag(name = "用户管理")
@RestController
@RequestMapping("/api/user")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/login")
    @Schema(description =  "用户登录")
    @Operation(summary = "用户登录", description = "用户登录")
    public R login(@RequestBody LoginDTO loginDTO) {
        try {
            LoginVO loginSuccess = userService.login(loginDTO);
            if (loginSuccess == null) {
                throw new BusinessException("账户密码错误");
            }
            return R.buildSuccess(loginSuccess);
        }catch (Exception e){
            throw new BusinessException("账户密码错误");
        }
    }


    @PostMapping("/addUser")
    @Schema(description =  "添加用户")
    @Operation(summary = "添加用户", description = "添加用户")
    public R addUser(@RequestBody UserDTO userDTO) {
         return R.buildSuccess(userService.addUser(userDTO));

    }

    @PostMapping("/editUser")
    @Schema(description =  "修改用户")
    @Operation(summary = "修改用户", description = "修改用户")
    public R editUser(@RequestBody UserDTO userDTO) {
        return R.buildSuccess(userService.editUser(userDTO));

    }


    @PostMapping("/delUser")
    @Schema(description =  "删除用户")
    @Operation(summary = "删除用户", description = "删除用户")
    public R delUser(@RequestParam Long id) {
        return R.buildSuccess(userService.delUser(id));

    }

    @PostMapping("/userPage")
    @Operation(summary = "用户分页", description = "用户分页")
    @ApiOperationSupport(author = "xdf", order = 1)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "0", description = "成功", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = List.class))
            })
    })
    public Page<UserPageVO> userPage(@RequestBody UserPageDTO userPageDTO,
                                     LUser lUser) {
        Page<UserPageVO> page = new Page<>(userPageDTO.getCurrent(), userPageDTO.getSize());
        return userService.userPage(page,userPageDTO,lUser);
    }
}
