package com.gxhl.gxplm.controller;

import com.gxhl.gxplm.common.exception.BusinessException;
import com.gxhl.gxplm.common.util.response.R;
import com.gxhl.gxplm.entity.vo.UploadVO;
import com.gxhl.gxplm.service.UploadService;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Slf4j
@Tag(name = "上传文件管理")
@RestController
@RequestMapping("/api/upload")
public class UplaodController {

    @Resource
    private  UploadService uploadService;

    @Value("${spring.servlet.multipart.location}")
    public String fileSavePath;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd/");




    @PostMapping("/uploadOld")
    @Schema(description =  "上传图片文件")
    public R<UploadVO> uploadImg(@RequestParam("file") MultipartFile file, String id) {
        UploadVO result = new UploadVO();
        String uploadUrl = uploadService.upload(file);
        result.setUrl(uploadUrl);
        return R.buildSuccess(result);
    }


    @PostMapping("/upload")
    @Schema(description =  "上传图片文件")
    public R<UploadVO> upload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        if (file.isEmpty()) {
            throw new BusinessException("请上传文件！");
        }
        String directory = simpleDateFormat.format(new Date());
        /**
         *  2.文件保存目录  E:/images/2020/03/15/
         *  如果目录不存在，则创建
         */
        File dir = new File(fileSavePath + directory);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        //3.给文件重新设置一个名字
        //后缀
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        String newFileName= UUID.randomUUID().toString().replaceAll("-", "")+suffix;
        //4.创建这个新文件
        File newFile = new File(fileSavePath + directory + newFileName);
        try {
            // 保存文件
            file.transferTo(newFile);
            String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/" + directory + newFileName;
            UploadVO uploadVO = new UploadVO();
            uploadVO.setUrl(url);
            return R.buildSuccess(uploadVO);
        } catch (IOException e) {
            e.printStackTrace();
           throw new BusinessException("上传失败！");
        }
    }
}
