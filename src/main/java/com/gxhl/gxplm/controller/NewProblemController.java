package com.gxhl.gxplm.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.gxhl.gxplm.common.util.response.R;
import com.gxhl.gxplm.config.LUser;
import com.gxhl.gxplm.entity.dto.AddProblemDTO;
import com.gxhl.gxplm.entity.dto.DelBatchProblemDTO;
import com.gxhl.gxplm.entity.dto.EditProblemDTO;
import com.gxhl.gxplm.entity.dto.NewProblemPageDTO;
import com.gxhl.gxplm.entity.vo.NewProblemPageVO;
import com.gxhl.gxplm.service.NewProblemService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@Tag(name = "新问题管理")
@RestController
@RequestMapping("/api/newProblem")
@AllArgsConstructor
public class NewProblemController {

    private final NewProblemService newProblemService;

    @PostMapping("/problemPage")
    @Operation(summary = "问题分页", description = "问题分页")
    @ApiOperationSupport(author = "xdf", order = 1)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "0", description = "成功", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = List.class))
            })
    })
    public Page<NewProblemPageVO> newProblemPage(@RequestBody NewProblemPageDTO newProblemPageDTO,
                                                 LUser lUser) {
        Page<NewProblemPageVO> page = new Page<>(newProblemPageDTO.getCurrent(), newProblemPageDTO.getSize());
        return newProblemService.newProblemPage(page,newProblemPageDTO,lUser);
    }


    @PostMapping("/problemList")
    @Operation(summary = "问题列表", description = "问题列表")
    @ApiOperationSupport(author = "xdf", order = 1)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "0", description = "成功", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = List.class))
            })
    })
    public List<NewProblemPageVO> problemList(@RequestBody NewProblemPageDTO newProblemPageDTO,LUser lUser) {
        return newProblemService.problemList(newProblemPageDTO,lUser);
    }

    @PostMapping("/addProblemParent")
    @Schema(description =  "添加问题")
    @Operation(summary = "添加问题", description = "添加问题")
    public R addNewProblem(@RequestBody AddProblemDTO addNewProblemDTO, LUser lUser) {
        return R.buildSuccess(newProblemService.addNewProblem(addNewProblemDTO,lUser));
    }


    @PostMapping("/editProblemParent")
    @Schema(description =  "修改问题")
    @Operation(summary = "修改问题", description = "修改问题")
    public R editProblemParent(@RequestBody EditProblemDTO editProblemDTO, LUser lUser) {
        return R.buildSuccess(newProblemService.editNewProblem(editProblemDTO,lUser));
    }

    @PostMapping("/delProblemParent")
    @Schema(description =  "删除问题")
    @Operation(summary = "删除问题", description = "删除问题")
    public R delNewProblem(@RequestParam Long id, LUser lUser) {
        return R.buildSuccess(newProblemService.delNewProblem(id,lUser));
    }

    @PostMapping("/delBatchProblem")
    @Schema(description =  "批量删除问题")
    @Operation(summary = "批量删除问题", description = "批量删除问题")
    public R delBatchProblem(@RequestBody DelBatchProblemDTO delBatchProblemDTO ) {
        return R.buildSuccess(newProblemService.delBatchProblem(delBatchProblemDTO));
    }


    @GetMapping("/countProblem")
    @Schema(description =  "查询问题总数")
    @Operation(summary = "查询问题总数", description = "查询问题总数")
    public R countProblem() {
        return R.buildSuccess(newProblemService.countProblem());
    }


    @Schema(description = "上传问题文件")
    @Operation(summary = "上传问题文件", description = "上传问题文件")
    @PostMapping("/uploadProblem")
    public Boolean uploadProblem(@RequestParam("file") MultipartFile file){
        return newProblemService.uploadProblem(file);
    }

    @Schema(description = "问题文件模版下载")
    @Operation(summary = "问题文件模版下载", description = "问题文件模版下载")
    @GetMapping("/downloadTemplate")
    public void downloadTemplate( HttpServletResponse response){
        newProblemService.downloadTemplate(response);
    }

    @Schema(description = "导出查询的问题报表")
    @Operation(summary = "导出查询的问题报表", description = "导出查询的问题报表")
    @GetMapping("/exportProblemList")
    public void exportProblemList(NewProblemPageDTO newProblemPageDTO, HttpServletResponse response,LUser lUser){
        newProblemService.exportProblemList(newProblemPageDTO,response,lUser);
    }

}
