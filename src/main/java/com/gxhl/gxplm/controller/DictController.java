package com.gxhl.gxplm.controller;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.gxhl.gxplm.common.util.response.R;
import com.gxhl.gxplm.entity.mybatis.Dict;
import com.gxhl.gxplm.service.DictService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 控制器
 *
 * @author Chill
 */
@Slf4j
@Tag(name = "字典管理")
@RestController
@AllArgsConstructor
@RequestMapping("/api/dict")
public class DictController {

	private DictService dictService;

	/**
	 * 获取字典
	 *
	 * @return
	 */
	@GetMapping("/dictionary")
	@ApiOperationSupport(order = 1)
	@Schema(description = "获取字典")
	@Operation(summary = "获取字典", description = "获取字典")
	public R<List<Dict>> dictionary(@RequestParam String code,Long parentId) {
		List<Dict> tree = dictService.getList(code,parentId);
		return R.buildSuccess(tree);
	}


}
