package com.gxhl.gxplm.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.gxhl.gxplm.common.util.response.R;
import com.gxhl.gxplm.config.LUser;
import com.gxhl.gxplm.entity.dto.ProblemPageDTO;
import com.gxhl.gxplm.entity.dto.ProblemParentDTO;
import com.gxhl.gxplm.entity.vo.ProblemPageVO;
import com.gxhl.gxplm.service.ProblemParentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Tag(name = "问题项管理")
@RestController
@RequestMapping("/api/problemParent")
@AllArgsConstructor
public class ProblemParentController {

    private final ProblemParentService problemParentService;

    @PostMapping("/addProblemParent")
    @Schema(description =  "添加问题")
    @Operation(summary = "添加问题", description = "添加问题")
    public R addProblem(@RequestBody ProblemParentDTO problemParentDTO, LUser lUser) {
        return R.buildSuccess(problemParentService.addProblemParent(problemParentDTO,lUser));
    }


    @PostMapping("/editProblemParent")
    @Schema(description =  "修改问题")
    @Operation(summary = "修改问题", description = "修改问题")
    public R editProblemParent(@RequestBody ProblemParentDTO problemParentDTO, LUser lUser) {
        return R.buildSuccess(problemParentService.editProblemParent(problemParentDTO,lUser));
    }

    @PostMapping("/delProblemParent")
    @Schema(description =  "删除问题")
    @Operation(summary = "删除问题", description = "删除问题")
    public R delProblemParent(@RequestParam Long id, LUser lUser) {
        return R.buildSuccess(problemParentService.delProblemParent(id,lUser));
    }

    @PostMapping("/problemPage")
    @Operation(summary = "问题项分页", description = "问题项分页")
    @ApiOperationSupport(author = "xdf", order = 1)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "0", description = "成功", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = List.class))
            })
    })
    public Page<ProblemPageVO> problemPage(@RequestBody ProblemPageDTO problemPageDTO,
                                    LUser lUser) {
        Page<ProblemPageVO> page = new Page<>(problemPageDTO.getCurrent(), problemPageDTO.getSize());
        return problemParentService.problemPage(page,problemPageDTO,lUser);
    }
}
