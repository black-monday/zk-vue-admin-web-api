package com.gxhl.gxplm.common.util;
import com.gxhl.gxplm.config.LUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


public class SecureUtil {
    public static LUser getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getDetails() instanceof LUser lUser) {
            return lUser;
        }
        return null;
    }
}
