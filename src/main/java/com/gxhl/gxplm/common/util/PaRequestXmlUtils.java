package com.gxhl.gxplm.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @Description: 平安XML请求报文处理公用类
 * @Author xdf
 * @Date 2021/8/27 16:10
 */
@Slf4j
public class PaRequestXmlUtils{

    /**
     * 创建XML格式请求报文
     *
     * @throws Exception
     */
    public static String entityToXml(Object obj, Boolean isUpperCase) {
        StringBuilder xml = new StringBuilder();
        xml.append("<?xml version='1.0' encoding='GBK'?><Result>");
        try {
            Class<?> aClass = obj.getClass();
            Field[] fields = aClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                String name = field.getName();
                if (isUpperCase) {
                    name = name.substring(0, 1).toUpperCase() + name.substring(1);
                } else {
                    name = name.substring(0, 1).toLowerCase() + name.substring(1);
                }
                Object o = ObjectUtils.isEmpty(field.get(obj)) ? "" : field.get(obj);
                Class<?> curFieldType = field.getType();
                xml.append("<").append(name).append(">");
                if (curFieldType.equals(List.class)) {
                    getListXMl(xml, field, (List) o);
                } else {
                    xml.append(o);
                }
                xml.append("</").append(name).append(">");
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        xml.append("</Result>");
        return xml.toString();
    }

    private static StringBuilder getListXMl(StringBuilder xml, Field field, List o) throws IllegalAccessException {
        Type genericType = field.getGenericType();
        if (null == genericType) {
            return xml;
        }
        if (genericType instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) genericType;
            Class<?> actualTypeArgument = (Class<?>) pt.getActualTypeArguments()[0];
            List<Object> lists = o;
            Field[] declaredFields = actualTypeArgument.getDeclaredFields();
            for (Object list : lists) {
                xml.append("<row>");
                for (Field declaredField : declaredFields) {
                    declaredField.setAccessible(true);
                    String rowName = declaredField.getName();
                    rowName = rowName.substring(0, 1).toUpperCase() + rowName.substring(1);
                    Object rowValue = ObjectUtils.isEmpty(declaredField.get(list)) ? "" : declaredField.get(list);
                    xml.append("<").append(rowName).append(">").append(rowValue).append("</").append(rowName).append(">");
                }
                xml.append("</row>");
            }
        }
        return xml;
    }
}
