package com.gxhl.gxplm.common.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @Author: xdf
 * @Description:
 * @Date: Created in 16:51 2023/2/1
 */
public class ToolsUtil {


    public static Date getPaymentDate(Integer paymentDay,Date giveMoneyDate){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(giveMoneyDate);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        // 合并日期
        calendar.set(year, month, paymentDay);
        Date paymentDate = calendar.getTime();
        System.out.println("合并后的日期：" + paymentDate);
        return paymentDate;
    }

    public static void main(String[] args) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String password = bCryptPasswordEncoder.encode("1234567");
        System.out.println(password);
    }
}
