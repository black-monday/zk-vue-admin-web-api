package com.gxhl.gxplm.common.util.log;


import org.slf4j.LoggerFactory;

public interface LoggerUtils {

    org.slf4j.Logger log = LoggerFactory.getLogger(LoggerUtils.class);
}
