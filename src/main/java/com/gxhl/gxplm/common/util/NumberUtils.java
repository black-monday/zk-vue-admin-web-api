package com.gxhl.gxplm.common.util;

import java.math.BigDecimal;

public class NumberUtils {

    /**
     * 两数相除
     *
     * @param divisor  除数
     * @param dividend 被除数
     * @param scale    小数点精度
     * @return
     */
    public static BigDecimal divide(BigDecimal divisor, BigDecimal dividend, int scale) {
        if (divisor == null || dividend == null || dividend.compareTo(new BigDecimal("0")) == 0) {
            return new BigDecimal("0.00");
        }
        return divisor.divide(dividend, scale, BigDecimal.ROUND_HALF_EVEN);
    }


    public static long multiply(BigDecimal divisor) {
        BigDecimal dividend = BigDecimal.valueOf(100);
        BigDecimal multiply = divisor.multiply(dividend);
        return multiply.longValue();
    }


    /**
     * 两数相除
     *
     * @param divisor  除数
     * @param dividend 被除数
     * @param scale    小数点精度
     * @return
     */
    public static BigDecimal divide(Integer divisor, Integer dividend, int scale) {
        if (divisor == null || dividend == null || dividend.compareTo(0) == 0) {
            return new BigDecimal("0.00");
        }
        return divide(new BigDecimal(divisor), new BigDecimal(dividend), scale);
    }

    /**
     * 两数相除
     * @param divisor  除数
     * @param dividend 被除数
     * @param scale    小数点精度
     * @return
     */
    public static BigDecimal divide(Long divisor, Long dividend, int scale) {
        if (divisor == null || dividend == null || dividend.compareTo(0L) == 0) {
            return new BigDecimal("0.00");
        }
        return divide(new BigDecimal(divisor), new BigDecimal(dividend), scale);
    }
}
