package com.gxhl.gxplm.common.log;

import org.springframework.context.ApplicationEvent;

/**
 * @Author: xdf
 * @Description:
 * @Date: Created in 16:51 2023/2/15
 */
public class LogAopEvent extends ApplicationEvent {
    private String desc;
    private int cost;
    private String ip;
    private String className;
    private String medthodName;
    private String params;
    private String resDesc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMedthodName() {
        return medthodName;
    }

    public void setMedthodName(String medthodName) {
        this.medthodName = medthodName;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getResDesc() {
        return resDesc;
    }

    public void setResDesc(String resDesc) {
        this.resDesc = resDesc;
    }


    public LogAopEvent(Object source, String desc, int cost, String ip, String className, String medthodName, String params, String resDesc) {
        super(source);
        this.desc = desc;
        this.cost = cost;
        this.ip = ip;
        this.className = className;
        this.medthodName = medthodName;
        this.params = params;
        this.resDesc = resDesc;
    }
}
