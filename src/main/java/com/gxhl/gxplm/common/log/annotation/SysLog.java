package com.gxhl.gxplm.common.log.annotation;

import java.lang.annotation.*;

/**
 * @Desc 系统日志注解
 * @Date 2022/3/1 12:55
 * @Created by wlh
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    String value() default "";
}
