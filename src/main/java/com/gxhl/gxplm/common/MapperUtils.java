package com.gxhl.gxplm.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Author: xdf
 * @Description:
 * @Date: Created in 15:05 2022/7/23
 */
@Slf4j
public class MapperUtils{
	private static ObjectMapper mapper = new ObjectMapper(new JsonFactory());

	static {
		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public static<T> T readObject(String json, Class<T> model){
		try {
			return mapper.readValue(json, model);
		} catch (IOException e) {
			log.error("readObject",e);
		}
		return null;
	}

	public static<T> List<T> readObjects(String json, Class<T> model)  {
		JavaType javaType = mapper.getTypeFactory().constructParametricType(List.class, model);
		try {
			return mapper.readValue(json, javaType);
		} catch (IOException e) {
			log.error("readObjects",e);
		}
		return null;
	}

	public static Boolean isValidJson(String json) {
		try {
			mapper.readTree(json);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static String writeMap(Map<String, Object> map) {
		try {
			return mapper.writeValueAsString(map);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String writeMapStr(Map<String, String> map) {
		try {
			return mapper.writeValueAsString(map);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String writeObject(Object obj) {
		try {
			return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
}
