package com.gxhl.gxplm.common.exception;

import com.gxhl.gxplm.common.util.response.R;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Date 2022/4/20 11:03
 * @Created by wlh
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public R handleException(Exception e, HttpServletRequest request) {
        log.error(request.getRequestURI() + ":服务运行异常------> {}", e);
        return R.buildFail("服务运行异常" + e.getMessage());
    }


    /**
     * 自定义异常处理
     */
    @ExceptionHandler(BusinessException.class)
    public R handleException(BusinessException e, HttpServletRequest request) {
        log.error(request.getRequestURI() + ":自定义内部异常------> {}", e);
        return R.buildFail(e.getMsg());
    }

    @ExceptionHandler(BindException.class)
    public R<?> handleBindException(BindException e) {
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        List<String> collect = fieldErrors.stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        return R.buildFail("参数校验失败,"+ collect);
    }
}
