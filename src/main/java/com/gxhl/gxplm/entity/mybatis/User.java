package com.gxhl.gxplm.entity.mybatis;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * <p>
 * 后台用户表
 * </p>
 *
 * @author wlh
 * @since 2022-04-20
 */
@Data
@TableName("op_user")
@Schema(description="后台用户表")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "用户表格主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(description = "roleType 用户所属角色类型 1超级管理员 2客户经理 3贷后管理人")
    private Integer roleType;

    @Schema(description = "用户昵称")
    private String name;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "密码")
    private String password;

    @Schema(description = "密码")
    private String mobile;

    @Schema(description = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @Schema(description = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @Schema(description = "用户状态 0-正常 1-禁用 2-删除")
    @TableField("status")
    private Integer status;

    @Schema(description = "部门名称")
    private String deptName;
}
