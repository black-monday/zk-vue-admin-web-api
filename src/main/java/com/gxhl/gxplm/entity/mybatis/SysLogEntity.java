package com.gxhl.gxplm.entity.mybatis;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 */
@Data
@TableName("sys_log")
@Schema(description="日志对象")
public class SysLogEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "操作人")
    @TableField("user_code")
    private String userCode;

    @Schema(description = "操作内容")
    @TableField("operation")
    private String operation;

    @Schema(description = "方法")
    @TableField("method")
    private String method;

    @Schema(description = "参数")
    @TableField("params")
    private String params;

    @Schema(description = "执行时长（毫秒）")
    @TableField("time")
    private Integer time;

    @Schema(description = "返回结果")
    @TableField("res")
    private String res;

    @Schema(description = "访问的ip地址")
    @TableField("ip")
    private String ip;

    @Schema(description = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @Schema(description = "创建时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
