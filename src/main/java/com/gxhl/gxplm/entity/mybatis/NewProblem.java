package com.gxhl.gxplm.entity.mybatis;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

@Data
@TableName("op_new_problem")
@Schema(description="问题")
public class NewProblem {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String problemType;

    private String problemDetail;

    private String corrective;

    private String dept;

    private String year;

    private String source;

    private String sourceType;

    private Long userId;

    private Date createTime;

    private Date updateTime;

    @Schema(description = "销号单")
    private String cancelNumberList;

}
