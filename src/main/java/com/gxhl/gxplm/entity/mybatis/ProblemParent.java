package com.gxhl.gxplm.entity.mybatis;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@TableName("op_problem_parent")
@Schema(description="问题项表")
public class ProblemParent {

    @Schema(description = "问题项主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(description = "整改类型1立行立改  2持续整改  3限期整改")
    private Integer actionType;

    @Schema(description = "计划完成时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date planFinishDate;

    @Schema(description = "完成时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date finishDate;

    @Schema(description = "审核销号时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date processTime;

    @Schema(description = "配合部门")
    private String coordinatDept;

    @Schema(description = "责任人")
    private String problemPerson;

    @Schema(description = "责任领导")
    private String problemLeader;


    @Schema(description = "责任部门")
    private String responsibleDept;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @Schema(description = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @Schema(description = "创建人")
    private Long createUser;

    @Schema(description = "更新人")
    private Long updateUser;

}
