package com.gxhl.gxplm.entity.mybatis;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@TableName("sys_menu")
@Data
public class Menu implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long parentId;

    private String code;

    private String name;

    private String alias;

    private String path;

    private String source;

    private Integer sort;

    private Integer category;

    private Integer action;

    private Integer isOpen;

    private String remark;

    @TableLogic
    private Integer isDeleted;
}
