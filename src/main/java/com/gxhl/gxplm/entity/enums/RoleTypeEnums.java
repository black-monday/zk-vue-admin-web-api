package com.gxhl.gxplm.entity.enums;

import lombok.Data;
import lombok.Getter;

@Getter
public enum RoleTypeEnums {
    SUPER_ADMIN(1, "超级管理员"),
    CUSTOMER_MANAGER(2, "客户经理"),
    LOANED_MANAGER(3, "贷后管理人");

    RoleTypeEnums(Integer code, String name){
        this.code = code;
        this.name = name;
    }

    private Integer code;

    private String name;

}
