package com.gxhl.gxplm.entity.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class NewProblemPageDTO {

    @Schema(description = "当前页")
    private Integer current;

    @Schema(description = "每页最大条数")
    private Integer size;

    @Schema(description = "问题分类")
    private String problemType;

    @Schema(description = "部门")
    private String dept;

    @Schema(description = "年份")
    private String year;

    @Schema(description = "来源")
    private String source;

    @Schema(description = "来源分类")
    private String sourceType;


}
