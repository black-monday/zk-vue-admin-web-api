package com.gxhl.gxplm.entity.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "添加或修改用户入参")
public class UserDTO {

    @Schema(description = "用户id")
    private Long id;

    @Schema(description = "用户姓名")
    private String name;

    @Schema(description = "用户账号")
    private String username;

    @Schema(description = "用户密码")
    private String password;

    @Schema(description = "手机号")
    private String mobile;

    @Schema(description = "用户角色 1超级管理员 2普通用户")
    private Integer roleType;

    @Schema(description = "部门")
    private String deptName;


}
