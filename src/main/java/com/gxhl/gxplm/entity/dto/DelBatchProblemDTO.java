package com.gxhl.gxplm.entity.dto;

import lombok.Data;

import java.util.List;

@Data
public class DelBatchProblemDTO {

    private List<Long> ids;
}
