package com.gxhl.gxplm.entity.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@Schema(description = "分页入参")
public class ProblemPageDTO {

    @Schema(description = "当前页")
    private Integer current;

    @Schema(description = "每页最大条数")
    private Integer size;

    @Schema(description = "责任领导")
    private String problemLeader;

    @Schema(description = "责任人")
    private String problemPerson;

    @Schema(description = "责任部门")
    private String responsibleDept;

    @Schema(description = "配合部门")
    private String coordinatDept;

    @Schema(description = "完成时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date finishDate;

    @Schema(description = "备注")
    private String remark;




}
