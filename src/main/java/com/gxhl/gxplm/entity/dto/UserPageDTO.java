package com.gxhl.gxplm.entity.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "用户分页")
public class UserPageDTO {

    @Schema(description = "当前页")
    private Integer current;

    @Schema(description = "每页最大条数")
    private Integer size;

    @Schema(description = "名称")
    private String name;

    @Schema(description = "手机号")
    private String mobile;


}
