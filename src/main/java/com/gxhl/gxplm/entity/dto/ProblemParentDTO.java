package com.gxhl.gxplm.entity.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
@Schema(description = "新增问题项入参")
public class ProblemParentDTO {

    @Schema(description = "问题项集合")
    private List<String> problems;

    @Schema(description = "整改措施集合")
    private List<String> actionNames;

    @Schema(description = "配合部门名称")
    private String coordinatDept;

    @Schema(description = "责任人名称")
    private  String personNames;

    @Schema(description = "责任领导")
    private String leaderNames;

    @Schema(description = "责任部门集合")
    private String responsibleDept;

    @Schema(description = "计划完成时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date planFinishDate;

    @Schema(description = "审核销号时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date processTime;

    @Schema(description = "完成时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date finishDate;

    @Schema(description = "整改类型1立行立改  2持续整改  3限期整改")
    private Integer actionType;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "问题父id,修改必传")
    private Long id;
    
}
