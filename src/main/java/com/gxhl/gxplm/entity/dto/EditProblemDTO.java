package com.gxhl.gxplm.entity.dto;

import com.gxhl.gxplm.entity.vo.UploadVO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
public class EditProblemDTO {

    @Schema(description = "问题id,修改必传")
    private Long id;

    @Schema(description = "问题分类")
    private String problemType;

    @Schema(description = "具体问题")
    private String problemDetail;

    @Schema(description = "整改措施")
    private String corrective;

    @Schema(description = "责任部门")
    private String dept;

    @Schema(description = "发生时间")
    private String year;

    @Schema(description = "来源")
    private String source;

    @Schema(description = "来源分类")
    private String sourceType;

    @Schema(description = "销号单")
    private List<UploadVO> cancelNumber;


}
