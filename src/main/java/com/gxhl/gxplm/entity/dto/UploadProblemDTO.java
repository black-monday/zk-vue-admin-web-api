package com.gxhl.gxplm.entity.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class UploadProblemDTO {

    @ExcelProperty(value = "序号", index = 0)
    private String no;

    @ExcelProperty(value = "问题分类", index = 1)
    private String problemType;

    @ExcelProperty(value = "具体问题", index = 2)
    private String problemDetail;

    @ExcelProperty(value = "整改措施", index = 3)
    private String corrective;

    @ExcelProperty(value = "责任部门", index = 4)
    private String dept;

    @ExcelProperty(value = "发生时间", index = 5)
    private String year;

    @ExcelProperty(value = "来源", index = 6)
    private String source;

    @ExcelProperty(value = "来源分类", index = 7)
    private String sourceType;

}
