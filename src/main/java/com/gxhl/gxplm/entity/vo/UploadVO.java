package com.gxhl.gxplm.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UploadVO implements Serializable {

    private String name;

    private String uid;
    
    private String url;
}
