package com.gxhl.gxplm.entity.vo;

import lombok.Data;

@Data
public class LoginVO {

    private String token;

    private String username;

    private String name;

    private String logo;

    private Integer roleType;

    private String deptName;

    private Long deptId;
}
