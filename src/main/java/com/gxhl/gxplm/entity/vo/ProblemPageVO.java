package com.gxhl.gxplm.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class ProblemPageVO {

    @Schema(description = "问题项父id")
    private Long id;

    @Schema(description = "问题项父id")
    private List<String> problems;

    @Schema(description = "问题项父id")
    private List<String> actionNames;

    @Schema(description = "整改类型1立行立改  2持续整改  3限期整改")
    private Integer actionType;

    @Schema(description = "计划完成时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date planFinishDate;

    @Schema(description = "完成时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date finishDate;

    @Schema(description = "配合部门")
    private String coordinatDept;

    @Schema(description = "责任人")
    private String problemPerson;

    @Schema(description = "责任领导")
    private String problemLeader;

    @Schema(description = "责任领导")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date processTime;


    @Schema(description = "责任部门")
    private String responsibleDept;

    @Schema(description = "备注")
    private String remark;





}
