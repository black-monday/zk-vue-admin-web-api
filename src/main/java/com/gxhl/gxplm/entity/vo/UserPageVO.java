package com.gxhl.gxplm.entity.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

@Data
@Schema(description = "用户分页")
public class UserPageVO {

    @Schema(description = "用户id")
    private Long id;

    @Schema(description = "用户名称")
    private String name;

    @Schema(description = "账号")
    private String username;

    @Schema(description = "用户角色类型")
    private Integer roleType;

    @Schema(description = "手机号")
    private String mobile;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "deptName")
    private String deptName;
}
