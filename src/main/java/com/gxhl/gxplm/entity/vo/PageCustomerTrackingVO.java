package com.gxhl.gxplm.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class PageCustomerTrackingVO {

    @Schema(description="跟进记录id")
    private Long id;

    @Schema(description="资方id")
    private Long bankId;

    @Schema(description="资方名称")
    private String bankName;

    @Schema(description = "贷款金额")
    private Long loanMoney;

    @Schema(description = "贷款金额")
    private BigDecimal loanMoneyDecimal;

    @Schema(description="当期还款金额")
    private Long currentPaymentMoney;

    @Schema(description="当期还款金额")
    private BigDecimal currentPaymentMoneyDecimal;

    @Schema(description="通知类型 当天。。。。")
    private Integer category;
    @Schema(description="还款日期")
    private Date paymentDate;

    @Schema(description="通知方式 1微信  2电话")
    private Integer type;

    @Schema(description="通知时间")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd hh:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd hh:mm:ss")
    private Date noticeTime;

    @Schema(description="通知备注")
    private String noticeRemark;




}
