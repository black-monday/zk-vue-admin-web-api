package com.gxhl.gxplm.entity.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class NewProblemPageVO {

    @Schema(description = "id")
    @ExcelProperty(index = 0)
    private Long id;

    @Schema(description = "问题分类")
    @ExcelProperty(index = 1)
    private String problemType;

    @Schema(description = "具体问题")
    @ExcelProperty(index = 2)
    private String problemDetail;

    @Schema(description = "措施")
    @ExcelProperty(index = 3)
    private String corrective;

    @Schema(description = "部门")
    @ExcelProperty(index = 4)
    private String dept;

    @Schema(description = "年份")
    @ExcelProperty(index = 5)
    private String year;

    @Schema(description = "来源")
    @ExcelProperty(index = 6)
    private String source;

    @Schema(description = "来源分类")
    @ExcelProperty(index = 7)
    private String sourceType;

    @ExcelIgnore
    private Long userId;

    @Schema(description = "创建时间")
    @DateTimeFormat(pattern="yyyy-MM-dd hh:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    @ExcelIgnore
    private Date createTime;

    @ExcelIgnore
    @Schema(description = "更新时间")
    @DateTimeFormat(pattern="yyyy-MM-dd hh:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    private Date updateTime;

    @ExcelIgnore
    private String cancelNumberList;

    @ExcelIgnore
    private List<UploadVO> cancelNumber;
}
