package com.gxhl.gxplm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableAspectJAutoProxy(proxyTargetClass = true, exposeProxy = true)
@SpringBootApplication(scanBasePackages = {"com.gxhl.gxplm"})
@MapperScan("com.gxhl.gxplm.mapper")
@EnableConfigurationProperties
@EnableTransactionManagement
@EnableAsync
public class GxLoanApplication {

    public static void main(String[] args) {
        SpringApplication.run(GxLoanApplication.class, args);
    }

}
