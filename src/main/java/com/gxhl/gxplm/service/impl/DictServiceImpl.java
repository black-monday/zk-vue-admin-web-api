package com.gxhl.gxplm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxhl.gxplm.entity.mybatis.Dict;
import com.gxhl.gxplm.mapper.DictMapper;
import com.gxhl.gxplm.service.DictService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements DictService {
    @Override
    public List<Dict> getList(String code,Long parentId) {
        return baseMapper.getList(code,parentId);
    }
}
