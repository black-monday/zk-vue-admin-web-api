package com.gxhl.gxplm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxhl.gxplm.entity.mybatis.CorrectiveActions;
import com.gxhl.gxplm.mapper.CorrectiveActionsMapper;
import com.gxhl.gxplm.service.CorrectiveActionsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CorrectiveActionsServiceImpl extends ServiceImpl<CorrectiveActionsMapper, CorrectiveActions> implements CorrectiveActionsService {
}
