package com.gxhl.gxplm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxhl.gxplm.entity.mybatis.Problem;
import com.gxhl.gxplm.mapper.ProblemMapper;
import com.gxhl.gxplm.service.ProblemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProblemServiceImpl extends ServiceImpl<ProblemMapper, Problem> implements ProblemService {
}
