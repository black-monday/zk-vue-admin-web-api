package com.gxhl.gxplm.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxhl.gxplm.common.MapperUtils;
import com.gxhl.gxplm.common.exception.BusinessException;
import com.gxhl.gxplm.config.LUser;
import com.gxhl.gxplm.entity.dto.*;
import com.gxhl.gxplm.entity.mybatis.NewProblem;
import com.gxhl.gxplm.entity.vo.NewProblemPageVO;
import com.gxhl.gxplm.entity.vo.UploadVO;
import com.gxhl.gxplm.listener.UploadProblemListener;
import com.gxhl.gxplm.mapper.NewProblemMapper;
import com.gxhl.gxplm.service.NewProblemService;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class NewProblemParentServiceImpl  extends ServiceImpl<NewProblemMapper, NewProblem> implements NewProblemService {
    @Override
    public Page<NewProblemPageVO> newProblemPage(Page<NewProblemPageVO> page, NewProblemPageDTO newProblemPageDTO, LUser lUser) {
        List<NewProblemPageVO> newProblemPage = this.baseMapper.newProblemPage(page, newProblemPageDTO, lUser.getDeptName());
        buildSelectVO(newProblemPage);
        return page.setRecords(newProblemPage);
    }

    private void buildSelectVO(List<NewProblemPageVO> newProblemPage) {
        if (CollectionUtils.isEmpty(newProblemPage)) {
            return;
        }
        newProblemPage.forEach(item->{
            item.setCancelNumber(new ArrayList<>());
            if (StringUtils.isNotEmpty(item.getCancelNumberList())){
                item.setCancelNumber(MapperUtils.readObjects(item.getCancelNumberList(), UploadVO.class));
            }
        });
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean uploadProblem(MultipartFile file) {
        //获取文件的输入流
        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        List<UploadProblemDTO> excelDataList = EasyExcel.read(inputStream) //调用read方法
                //注册自定义监听器，字段校验可以在监听器内实现
                .registerReadListener(new UploadProblemListener())
                .head(UploadProblemDTO.class) //对应导入的实体类
                .sheet(0) //导入数据的sheet页编号，0代表第一个sheet页，如果不填，则会导入所有sheet页的数据
                .headRowNumber(1) //列表头行数，1代表列表头有1行，第二行开始为数据行
                .doReadSync(); //开始读Excel，返回一个List<T>集合，继续后续入库操作
        if (CollectionUtils.isEmpty(excelDataList)){
            throw new BusinessException("请勿上传空数据");
        }
        List<NewProblem> resultList = new ArrayList<>();
        excelDataList.forEach(item->{
            NewProblem newProblem = new NewProblem();
            BeanUtils.copyProperties(item,newProblem);
            newProblem.setCreateTime(new Date());
            resultList.add(newProblem);
        });
        this.saveBatch(resultList);
        return true;
    }

    @Override
    public void downloadTemplate(HttpServletResponse response) {
        InputStream inputStream = null;
        try {
            response.reset();
            //设置输出文件格式
            String fileName = "模版文件";
            response.setContentType("application/vnd.ms-excel");
            response.addHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName, "utf-8") + ".xlsx");
            ServletOutputStream outputStream = response.getOutputStream();
            inputStream = this.getClass().getResourceAsStream("/templates/" + "problemTemplate.xlsx");
            byte[] buff = new byte[1024];
            int length;
            while ((length = inputStream.read(buff)) != -1) {
                outputStream.write(buff, 0, length);
            }
            if (outputStream != null) {
                outputStream.flush();
                outputStream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {

                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.error("关闭资源出错" + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object addNewProblem(AddProblemDTO addNewProblemDTO, LUser lUser) {
        NewProblem newProblem = new NewProblem();
        BeanUtils.copyProperties(addNewProblemDTO,newProblem);
        newProblem.setUserId(lUser.getUserId());
        newProblem.setCreateTime(new Date());
        if (!CollectionUtils.isEmpty(addNewProblemDTO.getCancelNumber())){
            newProblem.setCancelNumberList(MapperUtils.writeObject(addNewProblemDTO.getCancelNumber()));
        }else {
            newProblem.setCancelNumberList("");
        }
        return this.baseMapper.insert(newProblem);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object editNewProblem(EditProblemDTO editProblemDTO, LUser lUser) {
        if (Objects.isNull(editProblemDTO.getId())){
            throw new BusinessException("缺少id");
        }
        NewProblem newProblem = this.baseMapper.selectById(editProblemDTO.getId());
        if (Objects.isNull(newProblem)){
            throw new BusinessException("该问题已经不存在！");
        }
        BeanUtils.copyProperties(editProblemDTO,newProblem);
        newProblem.setUserId(lUser.getUserId());
        newProblem.setUpdateTime(new Date());
        if (!CollectionUtils.isEmpty(editProblemDTO.getCancelNumber())){
            newProblem.setCancelNumberList(MapperUtils.writeObject(editProblemDTO.getCancelNumber()));
        }else {
            newProblem.setCancelNumberList("");
        }
        return this.baseMapper.updateById(newProblem);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object delNewProblem(Long id, LUser lUser) {
        return this.removeById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object delBatchProblem(DelBatchProblemDTO delBatchProblemDTO) {
        if (CollectionUtils.isEmpty(delBatchProblemDTO.getIds())){
            throw new BusinessException("请选择要批量删除的问题！");
        }
        return this.removeBatchByIds(delBatchProblemDTO.getIds());
    }

    @Override
    public Object countProblem() {
        return this.baseMapper.countProblem();
    }

    @Override
    public List<NewProblemPageVO> problemList(NewProblemPageDTO newProblemPageDTO, LUser lUser) {
        List<NewProblemPageVO> newProblemPageVOS = this.baseMapper.problemList(newProblemPageDTO, lUser.getDeptName());
        buildSelectVO(newProblemPageVOS);
        return newProblemPageVOS;
    }

    @Override
    public void exportProblemList(NewProblemPageDTO newProblemPageDTO, HttpServletResponse response,LUser lUser) {
        List<NewProblemPageVO> resultList = this.problemList(newProblemPageDTO, lUser);
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        String fileName = "";
        try {
            fileName = URLEncoder.encode("报表结果", "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        response.setHeader("Content-disposition","attachment;filename=" + fileName + ".xlsx");
        response.setHeader("Content-Type","application/octet-stream;charset=utf-8");
        ClassPathResource couponOrderTemplateResource = new ClassPathResource("templates/problemTemplate.xlsx");
        try (ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).withTemplate(couponOrderTemplateResource.getInputStream()).build()) {
            WriteSheet sheet = EasyExcel.writerSheet(0, "汇总表").build();
            excelWriter.write(resultList, sheet);
            excelWriter.finish();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
