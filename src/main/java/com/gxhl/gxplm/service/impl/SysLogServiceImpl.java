package com.gxhl.gxplm.service.impl;

import com.gxhl.gxplm.common.log.LogAopEvent;
import com.gxhl.gxplm.entity.mybatis.SysLogEntity;
import com.gxhl.gxplm.mapper.SysLogMapper;
import com.gxhl.gxplm.service.SysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wlh
 * @since 2022-04-19
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLogEntity> implements SysLogService, ApplicationListener<LogAopEvent> {

    @Async
    @Override
    public void onApplicationEvent(LogAopEvent event) {
        SysLogEntity sysLogEntity = new SysLogEntity();
        sysLogEntity.setIp(event.getIp());
        sysLogEntity.setMethod(event.getMedthodName());
        sysLogEntity.setParams(event.getParams());
        sysLogEntity.setRes(event.getResDesc());
        sysLogEntity.setTime(event.getCost());
        sysLogEntity.setOperation(event.getDesc());
        this.baseMapper.insert(sysLogEntity);

    }
}
