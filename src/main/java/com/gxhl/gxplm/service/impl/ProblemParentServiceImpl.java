package com.gxhl.gxplm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxhl.gxplm.config.LUser;
import com.gxhl.gxplm.entity.dto.ProblemPageDTO;
import com.gxhl.gxplm.entity.dto.ProblemParentDTO;
import com.gxhl.gxplm.entity.mybatis.*;
import com.gxhl.gxplm.entity.vo.ProblemPageVO;
import com.gxhl.gxplm.mapper.ProblemParentMapper;
import com.gxhl.gxplm.service.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProblemParentServiceImpl extends ServiceImpl<ProblemParentMapper, ProblemParent> implements ProblemParentService {

    private final ProblemService problemService;
    
    private final CorrectiveActionsService correctiveActionsService;

    @Override
    public Page<ProblemPageVO> problemPage(Page<ProblemPageVO> page, ProblemPageDTO problemPageDTO, LUser lUser) {
        List<ProblemPageVO> problemPage = this.baseMapper.problemPage(page, problemPageDTO, lUser.getUserId());
        if (CollectionUtils.isEmpty(problemPage)) {
            return new Page<ProblemPageVO>();
        }
        List<Long> problemParentIds = problemPage.stream().map(ProblemPageVO::getId).toList();
        List<Problem> problemList = problemService.list(new LambdaQueryWrapper<Problem>().in(Problem::getProblemParentId, problemParentIds));
        List<CorrectiveActions> correctiveActionsList = correctiveActionsService.list(new LambdaQueryWrapper<CorrectiveActions>().in(CorrectiveActions::getProblemParentId, problemParentIds));
        Map<Long, List<Problem>> problemMap = problemList.stream().collect(Collectors.groupingBy(Problem::getProblemParentId));
        Map<Long, List<CorrectiveActions>> correctiveActionMap = correctiveActionsList.stream().collect(Collectors.groupingBy(CorrectiveActions::getProblemParentId));
        problemPage.forEach(item->{
            if (problemMap.containsKey(item.getId())){
                List<String> problems = problemMap.get(item.getId()).stream().map(Problem::getProblemName).collect(Collectors.toList());
                item.setProblems(problems);
            }
            if (correctiveActionMap.containsKey(item.getId())){
                List<String> actionNames = correctiveActionMap.get(item.getId()).stream().map(CorrectiveActions::getActionName).collect(Collectors.toList());
                item.setActionNames(actionNames);
            }
        });
        return page.setRecords(problemPage);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object editProblemParent(ProblemParentDTO problemParentDTO, LUser lUser) {
        //删除原来的 再重新添加
        this.delProblemParent(problemParentDTO.getId(),lUser);
        return this.addProblemParent(problemParentDTO,lUser);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object delProblemParent(Long id, LUser lUser) {
        this.removeById(id);
        problemService.remove(new LambdaQueryWrapper<Problem>().eq(Problem::getProblemParentId, id));
        correctiveActionsService.remove(new LambdaQueryWrapper<CorrectiveActions>().eq(CorrectiveActions::getProblemParentId,id));
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object addProblemParent(ProblemParentDTO problemDTO, LUser lUser) {
        //构建父问题
        ProblemParent problemParent = buildProblemParent(problemDTO,lUser);
        this.save(problemParent);
        problemDTO.setId(problemParent.getId());
        //构建问题项
        List<Problem> problems = buildProblems(problemDTO,lUser);
        problemService.saveBatch(problems);
        //构建整改措施
        List<CorrectiveActions> correctiveActions = buildCorrectiveActions(problemDTO,lUser);
        correctiveActionsService.saveBatch(correctiveActions);
        return true;
    }



    private List<Problem> buildProblems(ProblemParentDTO problemDTO, LUser lUser) {
        List<Problem> problemList = new ArrayList<>();
        List<String> problems = problemDTO.getProblems();
        problems.forEach(item->{
            Problem problem = new Problem();
            problem.setProblemName(item);
            problem.setCreateUser(lUser.getUserId());
            problem.setProblemParentId(problemDTO.getId());
            problemList.add(problem);
        });
        return problemList;
    }

    private List<CorrectiveActions> buildCorrectiveActions(ProblemParentDTO problemDTO, LUser lUser) {
        List<CorrectiveActions> list = new ArrayList<>();
        List<String> actionNames = problemDTO.getActionNames();
        actionNames.forEach(item->{
            CorrectiveActions actions = new CorrectiveActions();
            actions.setActionName(item);
            actions.setProblemParentId(problemDTO.getId());
            actions.setCreateUser(lUser.getUserId());
            list.add(actions);
        });
        return list;
    }


    private ProblemParent buildProblemParent(ProblemParentDTO problemDTO, LUser lUser){
        ProblemParent problemParent = new ProblemParent();
        problemParent.setCreateUser(lUser.getUserId());
        problemParent.setFinishDate(problemDTO.getFinishDate());
        problemParent.setPlanFinishDate(problemDTO.getPlanFinishDate());
        problemParent.setActionType(problemDTO.getActionType());
        problemParent.setRemark(problemDTO.getRemark());
        problemParent.setProblemPerson(problemDTO.getPersonNames());
        problemParent.setProblemLeader(problemDTO.getLeaderNames());
        problemParent.setResponsibleDept(problemDTO.getResponsibleDept());
        problemParent.setCoordinatDept(problemDTO.getCoordinatDept());
        problemParent.setProcessTime(problemDTO.getProcessTime());
        return problemParent;
    }
}
