package com.gxhl.gxplm.service.impl;

import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import com.gxhl.gxplm.config.oss.OSSConfig;
import com.gxhl.gxplm.service.UploadService;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.UUID;

@Service
@Slf4j
public class UploadServiceImpl implements UploadService {

    @Resource
    private OSSConfig ossConfig;

    @Override
    public String upload(MultipartFile file) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(file.getSize());
        objectMetadata.setContentType(file.getContentType());
        String ext = FilenameUtils.getExtension(file.getOriginalFilename());
        String uuid = UUID.randomUUID().toString();
        String url= MessageFormat.format("{0}_{1}/{2}.{3}","gxhl","gxzk",uuid,ext);
        PutObjectRequest putObjectRequest = null;
        try {
            putObjectRequest = new PutObjectRequest(ossConfig.getBucket(),url, file.getInputStream(), objectMetadata);
        } catch (IOException e) {
            log.info("上传文件异常：{}",e);
        }
        ossConfig.getClient().putObject(putObjectRequest);
        url = OSSConfig.baseUrl +  url;
        return url;
    }

}
