package com.gxhl.gxplm.service.impl;

import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxhl.gxplm.common.exception.BusinessException;
import com.gxhl.gxplm.config.LUser;
import com.gxhl.gxplm.config.jwt.JwtTokenProvider;
import com.gxhl.gxplm.entity.dto.LoginDTO;
import com.gxhl.gxplm.entity.dto.UserDTO;
import com.gxhl.gxplm.entity.dto.UserPageDTO;
import com.gxhl.gxplm.entity.enums.RoleTypeEnums;
import com.gxhl.gxplm.entity.mybatis.Dict;
import com.gxhl.gxplm.entity.mybatis.User;
import com.gxhl.gxplm.entity.vo.LoginVO;
import com.gxhl.gxplm.entity.vo.ProblemPageVO;
import com.gxhl.gxplm.entity.vo.UserPageVO;
import com.gxhl.gxplm.mapper.DictMapper;
import com.gxhl.gxplm.mapper.UserMapper;
import com.gxhl.gxplm.service.UserService;
import com.gxhl.gxplm.common.util.response.R;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 后台用户表 服务实现类
 * </p>
 * @author xdf
 * @since 2022-04-20
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {


    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final DictMapper dictMapper;

    @Override
    public LoginVO login(LoginDTO loginDTO) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword()));
        // 验证通过，设置授权信息至SecurityContextHolder
        if (authentication == null) {
            throw new BusinessException("账号密码错误");
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // 如果验证通过了，从返回的authentication里获得完整的UserDetails信息
        LUser userDetails = (LUser) authentication.getPrincipal();
        // 将用户的ID、名称等信息保存在jwt的token中
        String token = jwtTokenProvider.createToken(JSON.toJSONString(userDetails));
        LoginVO ls = new LoginVO();
        ls.setToken(token);
        ls.setUsername(userDetails.getUsername());
        ls.setName(userDetails.getName());
        ls.setRoleType(userDetails.getRoleType());
        //普通用户
        if (ls.getRoleType()  > 1){
            ls.setDeptName(userDetails.getDeptName());
            Dict dict = dictMapper.selectOne(new LambdaQueryWrapper<Dict>().eq(Dict::getDictValue, ls.getDeptName()));
            ls.setDeptId(dict.getId());
        }
        return ls;
    }

    @Override
    public Object addUser(UserDTO userDTO) {
        User one = this.getOne(new LambdaQueryWrapper<User>().eq(User::getUsername,userDTO.getUsername()).last("limit 1"));
        if (Objects.nonNull(one)){
            throw new BusinessException("该账号已经存在！请联系管理员!");
        }
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        User user = new User();
        user.setRoleType(userDTO.getRoleType());
        user.setName(userDTO.getName());
        user.setDeptName(userDTO.getDeptName());
        user.setUsername(userDTO.getUsername());
        user.setStatus(1);
        user.setCreateTime(new Date());
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        user.setMobile(userDTO.getMobile());
        return this.save(user);
    }

    @Override
    public Object editUser(UserDTO userDTO) {
        User one = this.getById(userDTO.getId());
        if (Objects.isNull(one)){
            throw new BusinessException("该账号不存在 或者 已经删除！请联系管理员!");
        }
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        one.setName(userDTO.getName());
        one.setUsername(userDTO.getUsername());
        one.setRoleType(userDTO.getRoleType());
        one.setDeptName(userDTO.getDeptName());
        if (Objects.nonNull(userDTO.getPassword())){
            one.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        }
        one.setUpdateTime(new Date());
        one.setMobile(userDTO.getMobile());
        return this.updateById(one);
    }

    @Override
    public Object delUser(Long id) {
        User one = this.getById(id);
        if (Objects.isNull(one)){
            throw new BusinessException("该账号不存在 或者 已经删除！请联系管理员!");
        }
        return this.removeById(id);
    }

    @Override
    public Page<UserPageVO> userPage(Page<UserPageVO> page, UserPageDTO userPageDTO, LUser lUser) {
        List<UserPageVO> userPage = this.baseMapper.userPage(page, userPageDTO, lUser.getUserId());
        if (CollectionUtils.isEmpty(userPage)) {
            return new Page<UserPageVO>();
        }
        return page.setRecords(userPage);
    }
}
