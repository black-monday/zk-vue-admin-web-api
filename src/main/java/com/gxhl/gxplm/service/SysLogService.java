package com.gxhl.gxplm.service;

import com.gxhl.gxplm.entity.mybatis.SysLogEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wlh
 * @since 2022-04-19
 */
public interface SysLogService extends IService<SysLogEntity>  {

}
