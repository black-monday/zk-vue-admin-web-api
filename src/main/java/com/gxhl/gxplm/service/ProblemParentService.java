package com.gxhl.gxplm.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxhl.gxplm.config.LUser;
import com.gxhl.gxplm.entity.dto.ProblemPageDTO;
import com.gxhl.gxplm.entity.dto.ProblemParentDTO;
import com.gxhl.gxplm.entity.mybatis.ProblemParent;
import com.gxhl.gxplm.entity.vo.ProblemPageVO;

public interface ProblemParentService extends IService<ProblemParent> {
    Object addProblemParent(ProblemParentDTO problemDTO, LUser lUser);

    Page<ProblemPageVO> problemPage(Page<ProblemPageVO> page, ProblemPageDTO problemPageDTO, LUser lUser);

    Object editProblemParent(ProblemParentDTO problemParentDTO, LUser lUser);

    Object delProblemParent(Long id, LUser lUser);
}
