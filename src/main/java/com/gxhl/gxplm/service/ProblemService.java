package com.gxhl.gxplm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxhl.gxplm.entity.mybatis.Problem;

public interface ProblemService extends IService<Problem> {
}
