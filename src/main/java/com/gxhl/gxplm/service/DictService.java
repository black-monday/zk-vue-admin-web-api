package com.gxhl.gxplm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxhl.gxplm.entity.mybatis.Dict;

import java.util.List;

public interface DictService extends IService<Dict> {
    List<Dict> getList(String code,Long parentId);
}
