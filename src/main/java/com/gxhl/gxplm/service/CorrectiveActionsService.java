package com.gxhl.gxplm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxhl.gxplm.entity.mybatis.CorrectiveActions;

public interface CorrectiveActionsService extends IService<CorrectiveActions> {
}
