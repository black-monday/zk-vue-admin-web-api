package com.gxhl.gxplm.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxhl.gxplm.config.LUser;
import com.gxhl.gxplm.entity.dto.LoginDTO;
import com.gxhl.gxplm.entity.dto.UserDTO;
import com.gxhl.gxplm.entity.dto.UserPageDTO;
import com.gxhl.gxplm.entity.mybatis.User;
import com.gxhl.gxplm.common.util.response.R;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxhl.gxplm.entity.vo.LoginVO;
import com.gxhl.gxplm.entity.vo.UserPageVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 后台用户表 服务类
 * </p>
 *
 * @author wlh
 * @since 2022-04-20
 */
public interface UserService extends IService<User> {

    LoginVO login(LoginDTO loginDTO);

    Object addUser(UserDTO userDTO);

    Object editUser(UserDTO userDTO);

    Object delUser(Long id);

    Page<UserPageVO> userPage(Page<UserPageVO> page, UserPageDTO userPageDTO, LUser lUser);
}
