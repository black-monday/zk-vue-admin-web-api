package com.gxhl.gxplm.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxhl.gxplm.config.LUser;
import com.gxhl.gxplm.entity.dto.AddProblemDTO;
import com.gxhl.gxplm.entity.dto.DelBatchProblemDTO;
import com.gxhl.gxplm.entity.dto.EditProblemDTO;
import com.gxhl.gxplm.entity.dto.NewProblemPageDTO;
import com.gxhl.gxplm.entity.mybatis.NewProblem;
import com.gxhl.gxplm.entity.vo.NewProblemPageVO;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface NewProblemService extends IService<NewProblem> {
    Page<NewProblemPageVO> newProblemPage(Page<NewProblemPageVO> page, NewProblemPageDTO newProblemPageDTO, LUser lUser);

    Boolean uploadProblem(MultipartFile file);

    void downloadTemplate(HttpServletResponse response);

    Object addNewProblem(AddProblemDTO addNewProblemDTO, LUser lUser);

    Object editNewProblem(EditProblemDTO editProblemDTO, LUser lUser);

    Object delNewProblem(Long id, LUser lUser);

    Object delBatchProblem(DelBatchProblemDTO delBatchProblemDTO);

    Object countProblem();

    List<NewProblemPageVO> problemList(NewProblemPageDTO newProblemPageDTO, LUser lUser);

    void exportProblemList(NewProblemPageDTO newProblemPageDTO, HttpServletResponse response,LUser lUser);
}
