package com.gxhl.gxplm.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxhl.gxplm.entity.dto.UserPageDTO;
import com.gxhl.gxplm.entity.mybatis.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxhl.gxplm.entity.vo.UserPageVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 后台用户表 Mapper 接口
 * </p>
 * @author xdf
 */
public interface UserMapper extends BaseMapper<User> {

    List<UserPageVO> userPage(Page<UserPageVO> page, @Param("params") UserPageDTO userPageDTO, Long userId);
}
