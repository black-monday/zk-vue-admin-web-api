package com.gxhl.gxplm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxhl.gxplm.entity.mybatis.Problem;

public interface ProblemMapper extends BaseMapper<Problem> {
}
