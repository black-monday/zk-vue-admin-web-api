package com.gxhl.gxplm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxhl.gxplm.entity.mybatis.CorrectiveActions;

public interface CorrectiveActionsMapper extends BaseMapper<CorrectiveActions> {
}
