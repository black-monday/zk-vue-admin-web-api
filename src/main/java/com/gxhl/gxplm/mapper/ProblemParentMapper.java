package com.gxhl.gxplm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxhl.gxplm.entity.dto.ProblemPageDTO;
import com.gxhl.gxplm.entity.mybatis.ProblemParent;
import com.gxhl.gxplm.entity.vo.ProblemPageVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProblemParentMapper extends BaseMapper<ProblemParent> {
    List<ProblemPageVO> problemPage(Page<ProblemPageVO> page, @Param("params") ProblemPageDTO problemPageDTO, Long userId);
}
