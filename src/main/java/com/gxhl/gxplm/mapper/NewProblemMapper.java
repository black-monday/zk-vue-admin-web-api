package com.gxhl.gxplm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxhl.gxplm.entity.dto.NewProblemPageDTO;
import com.gxhl.gxplm.entity.mybatis.NewProblem;
import com.gxhl.gxplm.entity.vo.NewProblemPageVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NewProblemMapper  extends BaseMapper<NewProblem> {
    List<NewProblemPageVO> newProblemPage(Page<NewProblemPageVO> page, @Param("params") NewProblemPageDTO newProblemPageDTO, @Param("deptName")String deptName);

    long countProblem();

    List<NewProblemPageVO> problemList(@Param("params") NewProblemPageDTO newProblemPageDTO, @Param("deptName") String deptName);
}
