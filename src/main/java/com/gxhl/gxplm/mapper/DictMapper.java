package com.gxhl.gxplm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxhl.gxplm.entity.mybatis.Dict;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DictMapper extends BaseMapper<Dict> {
    List<Dict> getList(String code,@Param("parentId") Long parentId);
}
