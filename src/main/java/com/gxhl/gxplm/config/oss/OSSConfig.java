package com.gxhl.gxplm.config.oss;

import com.aliyun.oss.OSSClient;
import com.gxhl.gxplm.config.GlobalConfig;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


@Configuration
public class OSSConfig {

    @Value("${oss.aliyun.accessId}")
    private String accessId;

    @Value("${oss.aliyun.accessKey}")
    private String accessKey;

    @Value("${oss.aliyun.endpoint}")
    private String endpoint;

    @Value("${oss.aliyun.bucket}")
    @Getter
    private String bucket;

    public static String baseUrl;

    @Getter
    private OSSClient client;

    @PostConstruct
    public void initOSSClient() {
        client=new OSSClient(endpoint, accessId, accessKey);
    }

    @Value("${oss.aliyun.baseUrl}")
    public void setBaseUrl(String baseUrl){
        OSSConfig.baseUrl =baseUrl;
        GlobalConfig.baseUrl=baseUrl;
    }
}
