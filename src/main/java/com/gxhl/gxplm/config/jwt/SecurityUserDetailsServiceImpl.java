package com.gxhl.gxplm.config.jwt;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gxhl.gxplm.config.LUser;
import com.gxhl.gxplm.entity.mybatis.User;
import com.gxhl.gxplm.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
@Component
@RequiredArgsConstructor
public class SecurityUserDetailsServiceImpl implements UserDetailsService {
    private final UserMapper userMapper;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userMapper.selectOne(new QueryWrapper<User>().lambda().eq(User::getUsername, username));
        LUser luser = new LUser();
        if (user != null) {
            luser.setName(user.getName());
            luser.setUsername(user.getUsername());
            luser.setPassword(user.getPassword());
            luser.setUserId(user.getId());
            luser.setRoleType(user.getRoleType());
            luser.setDeptName(user.getDeptName());
        } else {
            throw new UsernameNotFoundException(username);
        }
        return luser;
    }
}
