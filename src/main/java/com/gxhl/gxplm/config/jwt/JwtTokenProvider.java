package com.gxhl.gxplm.config.jwt;

import com.alibaba.fastjson2.JSON;
import com.gxhl.gxplm.config.LUser;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;
@Slf4j
@Component
@RequiredArgsConstructor
public class JwtTokenProvider {
    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expiration}")
    private long expire;
    @Value("${jwt.token-header}")
    private String tokenHeader;
    @Value("${jwt.token-prefix}")
    private String tokenPrefix;
    SecretKey key;

    @PostConstruct
    protected void init() {
        String secretKey = Base64.getEncoder().encodeToString(secret.getBytes());
        key = Keys.hmacShaKeyFor(secretKey.getBytes(StandardCharsets.UTF_8));
    }

    public String createToken(String userInfo) {

        Claims claims = Jwts.claims().setSubject(userInfo);
        Date validity = new Date(System.currentTimeMillis() + expire);
        return tokenPrefix + Jwts.builder()
                .setHeaderParam(Header.TYPE, Header.JWT_TYPE)
                .setClaims(claims)
                .setIssuedAt(new Date())
                .setExpiration(validity)
                .signWith(key)
                .compact();
    }

    public Authentication getAuthentication(String token) {
        String subject = getSubject(token);
        LUser userDetails = JSON.parseObject(subject, LUser.class);
        if (userDetails == null) {
            return null;
        }
        UsernamePasswordAuthenticationToken up = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
        up.setDetails(userDetails);
        return up;
    }

    public String getSubject(String token) {
        return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getSubject();
    }

    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader(tokenHeader);
        if (bearerToken == null) {
            bearerToken = req.getParameter(tokenHeader);
        }
        if (bearerToken != null && bearerToken.startsWith(tokenPrefix)) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public boolean validateToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (JwtException | IllegalArgumentException e) {
            log.warn("Expired or invalid JWT token");
        }
        return false;
    }
}
