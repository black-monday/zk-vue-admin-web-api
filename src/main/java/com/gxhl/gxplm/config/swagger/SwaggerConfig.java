package com.gxhl.gxplm.config.swagger;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.customizers.GlobalOpenApiCustomizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class SwaggerConfig {
    @Value("${springdoc.config.title}")
    private String title;
    @Value("${springdoc.config.version}")
    private String version;
    @Value("${springdoc.config.description}")
    private String description;
    @Value("${springdoc.config.author}")
    private String author;

    @Bean
    public GlobalOpenApiCustomizer orderGlobalOpenApiCustomizer() {
        return openApi -> {
            if (openApi.getTags() != null) {
                openApi.getTags().forEach(tag -> {
                    Map<String, Object> map = new HashMap<>();
                    tag.setExtensions(map);
                });
            }
        };
    }

    @Bean
    public OpenAPI customOpenAPI() {
        Contact ct = new Contact();
        ct.setName(author);
        OpenAPI oa = new OpenAPI()
                .info(new Info()
                        .title(title)
                        .version(version)
                        .description(description).contact(ct)
                        .termsOfService("http://127.0.0.1")
                        .license(new License().name("Apache 2.0")
                                .url("http://127.0.0.1")));

        return oa;
    }
}
